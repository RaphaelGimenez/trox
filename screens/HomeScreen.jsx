import * as React from 'react';
import {
  View, FlatList,
} from 'react-native';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';

import YoutubeApiV3Adapter from 'youtube-api-adapter';
import { useNavigation } from '@react-navigation/native';
import Video from '../components/Video';
import HeaderSearch from '../components/HeaderSearch';
import Conf from '../conf';

const YoutubeApi = new YoutubeApiV3Adapter(Conf.API_KEY);

export default function HomeScreen({ type }) {
  dayjs.extend(relativeTime);
  const [popMusic, setPopMusic] = React.useState([]);
  const [popGaming, setPopGaming] = React.useState([]);
  const navigation = useNavigation();

  React.useEffect(() => {
    if (type === 'gaming') {
      YoutubeApi.getVideos({ videoCategoryId: 20, regionCode: 'FR', chart: 'mostPopular' }).then((res) => {
        setPopGaming(res.items);
      });
    } else {
      YoutubeApi.getVideos({ videoCategoryId: 10, regionCode: 'FR', chart: 'mostPopular' }).then((res) => {
        setPopMusic(res.items);
      });
    }
  }, [type]);

  const renderItem = (data) => (
    <Video
      onPressHandler={(() => {
        navigation.navigate('Video', { id: data.item.id });
      })}
      thumbnail={data.item.snippet.thumbnails.medium.url}
      channelTitle={data.item.snippet.channelTitle}
      title={data.item.snippet.title}
      publishedAt={data.item.snippet.publishedAt}
    />
  );

  return (
    <View>
      <HeaderSearch />
      <FlatList
        data={type === 'gaming' ? popGaming : popMusic}
        keyExtractor={(item) => item.id}
        renderItem={renderItem}
      />
    </View>
  );
}

HomeScreen.propTypes = {
  type: PropTypes.string.isRequired,
};

HomeScreen.navigationOptions = {
  header: null,
};
