import React, { useState } from 'react';
import { FlatList } from 'react-native';
import { useRoute, useNavigation } from '@react-navigation/native';
import YoutubeApiV3Adapter from 'youtube-api-adapter';
import Video from '../components/Video';
import Conf from '../conf';

const YoutubeApi = new YoutubeApiV3Adapter(Conf.API_KEY);


export default function SearchScreen() {
  const [videos, setVideos] = useState();
  const route = useRoute();
  const navigation = useNavigation();

  React.useEffect(() => {
    YoutubeApi.search({ q: route.params.suggestion }).then((res) => {
      setVideos(res.items);
    });
  }, []);

  const renderItem = (data) => (
    <Video
      onPressHandler={(() => navigation.navigate('Video', { id: data.item.id.videoId }))}
      thumbnail={data.item.snippet.thumbnails.medium.url}
      channelTitle={data.item.snippet.channelTitle}
      title={data.item.snippet.title}
      publishedAt={data.item.snippet.publishedAt}
    />
  );
  return (
    <FlatList
      data={videos}
      keyExtractor={(item) => item.id.videoId}
      renderItem={renderItem}
    />
  );
}
