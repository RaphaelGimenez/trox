import React, { useState, useRef } from 'react';
import {
  Text, View, Platform, StyleSheet, Dimensions,
  ScrollView,
  Image,
} from 'react-native';
import { useRoute, useNavigation } from '@react-navigation/native';
import YoutubeApiV3Adapter from 'youtube-api-adapter';
import WebView from 'react-native-webview';
import { ButtonGroup, Icon } from 'react-native-elements';
import Video from '../components/Video';
import Comment from '../components/Comment';
import Conf from '../conf';

const YoutubeApi = new YoutubeApiV3Adapter(Conf.API_KEY);

const styles = StyleSheet.create({
  WebViewContainer: {
    width: '100%',
    height: '100%',
  },
});

const WIDTH = Platform.OS === 'ios' ? Dimensions.get('window').height : Dimensions.get('window').width;

export default function VideoScreen() {
  const [video, setVideo] = useState();
  const [channel, setChannel] = useState();
  const [comments, setComments] = useState([]);
  const [suggestions, setSuggestions] = useState([]);
  const [buttonsIndex, setButtonsIndex] = useState(0);
  const [showDescription, setShowDescription] = useState(false);
  const [likeWidth, setLikeWidth] = useState(0);
  const scrollViewRef = useRef();
  const navigation = useNavigation();
  const route = useRoute();
  const buttons = ['suggestions', 'commentaires'];

  React.useEffect(() => {
    YoutubeApi.getVideos({ id: route.params.id }).then((res) => {
      const currVideo = res.items[0];
      setVideo(currVideo);
      const left = currVideo.statistics.likeCount * 100;
      const right = +currVideo.statistics.likeCount + +currVideo.statistics.dislikeCount;
      setLikeWidth(
        (left / right).toFixed(),
      );
      YoutubeApi.getChannelById(res.items[0].snippet.channelId, false).then((currChannel) => {
        setChannel(currChannel.items[0]);
      });
    });
    YoutubeApi.getComments({ videoId: route.params.id }).then((res) => {
      setComments(res.items);
    });
    YoutubeApi.search({ relatedToVideoId: route.params.id }).then((res) => {
      setSuggestions(res.items);
    });
  }, [route.params.id]);

  const updateIndex = (index) => {
    setButtonsIndex(index);
  };

  const computedView = (views) => (views > 9999 ? `${(views / 1000).toFixed(1)}k` : views);

  const onPressHandler = (id) => {
    scrollViewRef.current.scrollTo({ y: 0 });
    navigation.navigate('Video', { id });
  };

  const renderVideo = (data) => (
    <Video
      key={data.id.videoId}
      onPressHandler={() => onPressHandler(data.id.videoId)}
      thumbnail={data.snippet.thumbnails.medium.url}
      channelTitle={data.snippet.channelTitle}
      title={data.snippet.title}
      publishedAt={data.snippet.publishedAt}
    />
  );

  const renderComment = (data) => {
    const {
      textDisplay,
      authorDisplayName,
      authorProfileImageUrl,
    } = data.snippet.topLevelComment.snippet;
    return (
      <Comment
        key={data.id}
        textDisplay={textDisplay}
        authorDisplayName={authorDisplayName}
        authorProfileImageUrl={authorProfileImageUrl}
      />
    );
  };

  return (
    <ScrollView ref={scrollViewRef}>
      <View style={{ height: (1080 * WIDTH) / 1920, backgroundColor: 'white' }}>
        <WebView
          style={styles.WebViewContainer}
          javaScriptEnabled
          domStorageEnabled
          source={{ uri: `https://www.youtube.com/embed/${route.params.id}` }}
        />
      </View>
      <View style={{ padding: 8, backgroundColor: 'white' }}>
        <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{video?.snippet.localized.title}</Text>
        <View style={{
          display: 'flex', flexDirection: 'row', alignItems: 'center', marginTop: 8,
        }}
        >
          <View style={{
            height: 45, width: 45, backgroundColor: 'grey', borderRadius: 50,
          }}
          >
            <Image
              style={{ height: '100%', width: '100%', borderRadius: 100 }}
              source={{ uri: channel?.snippet.thumbnails.default.url }}
            />
          </View>
          <Text style={{ fontSize: 14, fontWeight: 'bold', marginLeft: 8 }}>
            {video?.snippet.channelTitle}
          </Text>
          <Text style={{ fontSize: 12, marginLeft: 8 }}>
            {computedView(video?.statistics.viewCount)}
            {' '}
            vues
          </Text>
        </View>
        <View style={{
          flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 8,
        }}
        >
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <Icon style={{ marginRight: 4 }} size={14} name="thumb-up" type="material-community" />
            <Text>
              {computedView(video?.statistics.likeCount)}
            </Text>
          </View>
          <View style={{
            flex: 3, height: 15, backgroundColor: 'rgba(92, 99, 171, 0.3)', borderRadius: 3, position: 'relative',
          }}
          >
            <View style={{
              height: 15,
              position: 'absolute',
              left: 0,
              top: 0,
              backgroundColor: 'rgba(92, 99, 171, 1)',
              borderRadius: 3,
              width: `${likeWidth}%`,
            }}
            />
          </View>

          <View style={{
            flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end',
          }}
          >
            <Text style={{ textAlign: 'right' }}>{computedView(video?.statistics.dislikeCount)}</Text>
            <Icon style={{ marginLeft: 4 }} size={14} name="thumb-down" type="material-community" />
          </View>
        </View>
        {
          showDescription
            ? <Text style={{ marginTop: 8 }}>{video?.snippet.localized.description}</Text>
            : null
        }
        <Text
          style={{
            textAlign: 'center', fontSize: 14, fontWeight: 'bold', padding: 8,
          }}
          onPress={() => setShowDescription(!showDescription)}
        >
          {showDescription ? 'cacher' : 'afficher'}
          {' '}
          la description
        </Text>
      </View>
      <View>
        <ButtonGroup
          containerStyle={{
            width: '100%', marginLeft: 0, marginTop: 0, marginBottom: 0, borderRadius: 0, borderWidth: 0, height: 40,
          }}
          selectedButtonStyle={{ backgroundColor: '#5C63AB' }}
          onPress={updateIndex}
          selectedIndex={buttonsIndex}
          buttons={buttons}
        />
        <View>
          {
            buttonsIndex === 1
              ? comments.map((c) => renderComment(c))
              : suggestions.map((s) => renderVideo(s))

          }
        </View>
      </View>
    </ScrollView>
  );
}
