import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import { Image } from 'react-native-elements';

import HTML from 'react-native-render-html';

const Comment = ({ authorProfileImageUrl, authorDisplayName, textDisplay }) => (
  <View
    style={{
      display: 'flex', flexDirection: 'row', padding: 8, width: '100%', backgroundColor: 'white',
    }}
  >
    <View style={{
      height: 45, width: 45, backgroundColor: 'grey', borderRadius: 50,
    }}
    >
      <Image
        style={{ height: '100%', width: '100%', borderRadius: 100 }}
        source={{ uri: authorProfileImageUrl }}
      />
    </View>
    <View style={{ marginLeft: 8, width: '80%' }}>
      <Text style={{ fontWeight: 'bold' }}>{authorDisplayName}</Text>
      <HTML html={textDisplay} />
    </View>
  </View>
);

Comment.propTypes = {
  textDisplay: PropTypes.string.isRequired,
  authorDisplayName: PropTypes.string.isRequired,
  authorProfileImageUrl: PropTypes.string.isRequired,
};

export default Comment;
