import React from 'react';
import {
  View, Image,
  TextInput,
  TouchableOpacity,
  Text,
} from 'react-native';
import { Icon } from 'react-native-elements';
import YoutubeApiV3Adapter from 'youtube-api-adapter';
import { useNavigation } from '@react-navigation/native';
import Logo from '../assets/images/logo.png';
import Styles from './HeaderSearch.style';
import Conf from '../conf';

const YoutubeApi = new YoutubeApiV3Adapter(Conf.API_KEY);

const HeaderSearch = () => {
  const [expandSearchbar, setExpandSearchbar] = React.useState(false);
  const [search, setSearch] = React.useState('');
  const [suggestions, setSuggestions] = React.useState([]);
  const navigation = useNavigation();

  const onChangeText = async (nextSearch) => {
    setSearch(nextSearch);
    if (nextSearch.length) {
      const newSuggestions = await YoutubeApi.getSuggestions(nextSearch);
      newSuggestions.length = 5;
      setSuggestions(newSuggestions);
    } else {
      setSuggestions([]);
    }
  };

  const onSuggestionSelected = (suggestion) => {
    setSuggestions([]);
    setSearch('');
    navigation.navigate('Search', { suggestion });
  };

  return (
    <View style={{ overflowY: 'visible', elevation: 100 }}>
      <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
        <Image
          style={{
            marginLeft: expandSearchbar ? 0 : 16,
            height: 20,
            width: expandSearchbar ? 0 : 60,
          }}
          source={Logo}
        />

        <View style={Styles.searchBarWrapper}>
          <Icon
            containerStyle={Styles.iconContainerStyle}
            name="magnify"
            type="material-community"
          />
          <TextInput
            returnKeyType="search"
            style={Styles.searchBar}
            onChangeText={onChangeText}
            onSubmitEditing={() => onSuggestionSelected(search)}
            value={search}
            placeholder="Rechercher une vidéo"
            onFocus={() => setExpandSearchbar(true)}
            onBlur={() => setExpandSearchbar(false)}
          />
        </View>
      </View>
      {
        suggestions.length
          ? (
            <View style={Styles.suggestions}>
              {
        suggestions.map((l) => (
          <TouchableOpacity
            key={l}
            onPress={() => onSuggestionSelected(l)}
          >
            <View
              style={Styles.suggestionItem}
            >
              <Text>{l}</Text>
              <Icon name="arrow-top-left-thick" type="material-community" />
            </View>
          </TouchableOpacity>
        ))
      }
            </View>
          )
          : null
      }
    </View>
  );
};

export default HeaderSearch;
