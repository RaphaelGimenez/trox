import * as React from 'react';
import {
  Text, View, Image,
  Dimensions, StyleSheet,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';


const styles = StyleSheet.create({
  videoItemImage: {
    height: Dimensions.get('window').height / 4,
    width: '100%',
  },
});

const Video = ({
  thumbnail, channelTitle, title, publishedAt, onPressHandler,
}) => {
  dayjs.extend(relativeTime);
  return (
    <TouchableOpacity onPress={onPressHandler}>
      <View style={{ marginBottom: 16 }}>
        <Image
          style={styles.videoItemImage}
          source={{ uri: thumbnail }}
        />
        <View style={{ paddingLeft: 16, paddingTop: 8, paddingRight: 16 }}>
          <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{title}</Text>
          <Text>
            {channelTitle}
            ,
            {' '}
            {dayjs(publishedAt).fromNow()}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

Video.propTypes = {
  thumbnail: PropTypes.string.isRequired,
  channelTitle: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  publishedAt: PropTypes.string.isRequired,
  onPressHandler: PropTypes.func.isRequired,
};

export default Video;
