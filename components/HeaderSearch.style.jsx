import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  searchBarWrapper: {
    flex: 1,
    position: 'relative',
    backgroundColor: '#fff',
    margin: 16,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.15,
    shadowRadius: 4.65,
    elevation: 7,
  },
  searchBar: {
    height: 40,
    position: 'relative',
    zIndex: 1,
    paddingLeft: 40,
  },
  iconContainerStyle: {
    position: 'absolute',
    overflow: 'visible',
    zIndex: 100,
    left: 10,
    top: 8,
    width: 20,
    height: 20,
    opacity: 0.5,
  },
  suggestions: {
    elevation: 300,
    width: '100%',
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    padding: 4,
  },
  suggestionItem: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    borderRadius: 3,
    margin: 4,
    padding: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.15,
    shadowRadius: 2.22,
    elevation: 30,
  },
});
