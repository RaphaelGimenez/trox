# Trox
Trox est une application en react native utilisant l'api Youtube via l'adaptateur https://gitlab.com/RaphaelGimenez/youtube-api-adapter.
Trox permet de rechercher une vidéo sur youtube et la lire directement depuis l'application.
Il est possible de rechercher une vidéo, accéder à son détail, voir ses suggestions et commentaires et écouter avec l'écran éteint.

## Utilisation
modifier le fichier `./config.js` avec votre clé d'API

`npm i`
lancer expo :
`expo start`

l'apk construite est disponible au chemin suivant : `./trox.apk`


## Démo
https://youtu.be/nPxxKPuQj_A